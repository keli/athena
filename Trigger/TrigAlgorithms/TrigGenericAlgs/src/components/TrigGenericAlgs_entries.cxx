#include "../TimeBurner.h"
#include "../RandomErrorAlg.h"
#include "../EndOfEventROIConfirmerAlg.h"
#include "../EndOfEventPrescaleCheckAlg.h"

DECLARE_COMPONENT( TimeBurner )
DECLARE_COMPONENT( RandomErrorAlg )
DECLARE_COMPONENT( EndOfEventROIConfirmerAlg )
DECLARE_COMPONENT( EndOfEventPrescaleCheckAlg )
