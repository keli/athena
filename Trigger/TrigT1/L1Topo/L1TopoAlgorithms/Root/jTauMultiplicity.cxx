/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/
/*********************************
 * jTauMultiplicity.cpp
 * Created by Carlos Moreno on 05/06/20.
 *
 * @brief algorithm that computes the multiplicity for a specified list and ET threshold
 * line 1: 0 or 1, line 1 and 2 : 2 or more, uses 2 bits
 *
 * @param NumberLeading MinET

**********************************/

#include <cmath>

#include "L1TopoAlgorithms/jTauMultiplicity.h"
#include "L1TopoCommon/Exception.h"
#include "L1TopoInterfaces/Count.h"

#include "L1TopoEvent/TOBArray.h"
#include "L1TopoEvent/jTauTOBArray.h"
#include "L1TopoEvent/GenericTOB.h"

REGISTER_ALG_TCS(jTauMultiplicity)

using namespace std;


TCS::jTauMultiplicity::jTauMultiplicity(const std::string & name) : CountingAlg(name)
{
   
   
   setNumberOutputBits(12); //To-Do: Make this flexible to addapt to the menu. Each counting requires more than one bit

}

TCS::jTauMultiplicity::~jTauMultiplicity(){}


TCS::StatusCode
TCS::jTauMultiplicity::initialize() { 

  m_threshold = getThreshold();

  // book histograms
  bool isMult = true;

  std::string hname_accept = "hjTauMultiplicity_accept_EtaPt_"+m_threshold->name();
  bookHist(m_histAccept, hname_accept, "ETA vs PT", 150, -100, 100, 150, 0., 100., isMult);

  hname_accept = "hjTauMultiplicity_accept_counts_"+m_threshold->name();
  bookHist(m_histAccept, hname_accept, "COUNTS", 15, 0., 10., isMult);

  return StatusCode::SUCCESS;
     
}


TCS::StatusCode
TCS::jTauMultiplicity::processBitCorrect( const TCS::InputTOBArray & input,
					 Count & count)

{
   return process(input, count);
}

TCS::StatusCode
TCS::jTauMultiplicity::process( const TCS::InputTOBArray & input,
			       Count & count )
{

  // Grab the threshold and cast it into the right type
  auto jTAUThr = dynamic_cast<const TrigConf::L1Threshold_jTAU &>(*m_threshold);

  // Grab inputs
  const jTauTOBArray & jtaus = dynamic_cast<const jTauTOBArray&>(input);

  int counting = 0; 
  
  // loop over input TOBs
  for(jTauTOBArray::const_iterator jtau = jtaus.begin();
      jtau != jtaus.end();
      ++jtau ) {
    
    const GenericTOB gtob(**jtau);

    // Dividing by 4 standing for converting eta from 0.025 to 0.1 granularity as it is defined in the menu as 0.1 gran.
    bool passed = gtob.Et() >= jTAUThr.thrValue100MeV(gtob.eta()/4);

    if (passed) {
      counting++; 
      fillHist2D( m_histAccept[0], gtob.eta(), gtob.EtDouble() );
    }

  }

  fillHist1D( m_histAccept[1], counting);
  
  // Pass counting to TCS::Count object - output bits are composed there
  count.setSizeCount(counting);
  
  return TCS::StatusCode::SUCCESS;

}
